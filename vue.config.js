module.exports = {
  "runtimeCompiler": true,

  "transpileDependencies": [
    "vuetify"
  ],
  publicPath:"./",
  chainWebpack: config => {
    config.plugins.delete('prefetch');
    config.plugins.delete('preload');
  },
  configureWebpack: {
    optimization: {
      splitChunks: false
    }
  },
  productionSourceMap: false
};