import {auditConstructor} from "./auditConstructor/index.js";
import {auditResult} from "./auditResult/index.js";
import {auditChat} from "./auditChat/index.js";
export default {
    components: {
        auditConstructor,
        auditResult,
        auditChat,
    },
    props:{
        idProject: {},
    },
    computed:{
        // access(){
        //     return this.$store.state.auth.access.some(i=>i.id==this.idProject);
        // },
    },
    watch:{
        // access(){
        //     this.definePage();
        // },
    },
    data(){
        return {
            // domain:'http://hcapisystem:8083',
            domain: this.$store.state.api.urlApi,
            tabs: [
                {name:"Конструктор", value:"auditConstructor"},
                {name:"Результаты", value:"auditResult"},
            ],
            comp: null,
            page:'',
        };
    },
    mounted() {
        this.definePage();
    },
    methods:{
        definePage(){
            // if(!!Number(this.idProject) && this.access) {
            //     let get = window.location.hash.split('?')[1];
            //     if (get) {
            //         let getObj = this.getToObject(get);
            //         this.viewPage(getObj.comp, true);
            //         this.comp = this.tabs.findIndex(i => i.value == getObj.comp);
            //     } else {
                    let page = 'auditConstructor';
                    this.viewPage(page);
                    this.comp = this.tabs.findIndex(i => i.value == page);
            //     }
            // }
        },
        getToObject(get){
            let arr = get.split('&');
            let obj = {};
            for(let key in arr){
                let elem = arr[key].split('=');
                obj[elem[0]] = decodeURIComponent(elem[1]);
            }
            return obj;
        },
        viewPage(page,start = false){
            this.page = page;
            //if(!start)window.location.hash = window.location.hash.split('?')[0]+'?comp='+page;
        },
    },
    template:/*html*/`
        <div class="d-flex flex-column" style="height: 100%;">
                <v-tabs
                        fixed-tabs
                        __background-color="primary"
                        __dark
                        style="flex-grow: 0;"
                        v-model="comp"
                >
                    <v-tab
                            v-for="tab in tabs"
                            :key="tab.value"
                            @click="viewPage(tab.value)"
                    >
                        {{tab.name}}
                    </v-tab>
                </v-tabs>
                <v-divider/>
                <component
                        class="hc_audit_all flex-grow-1"
                        :is="page"
                        :domain="domain"
                        __viewPage="viewPage"
                        :idProject="Number(idProject)"
                ></component>
        </div>
    `
}