export const themesList = {
    props: {
        dataObject: Object,
        domain: String,
        idProject: Number,
    },
    methods: {
        initialize() {
            if(!this.showPreloader) {
                this.showPreloader = true;
                this.$post(this.domain, {
                    rout: 'gnktAuditApiAll',
                    routAuditApi: 'getThemes',
                    userID: this.$store.state.auth.info.id,
                    allUsers: this.allUsers,
                }).then(json => {
                    this.desserts = json;
                }).catch(() => {
                    console.error("Ошибка ответа сервера...");
                }).finally(() => {
                    this.showPreloader = false;
                });
            }
        },

        editItem(item) {
            this.editedIndex = this.desserts.indexOf(item)
            this.editedItem = Object.assign({}, item)
            this.dialog = true
        },

        deleteItem(item) {
            if(confirm('Вы уверены что хотите удалить тему?')) {
                const index = this.desserts.indexOf(item)
                this.statusDel = false;
                this.$post(this.domain, {
                    rout: 'gnktAuditApiAll',
                    routAuditApi: 'deleteTheme',
                    ID: this.desserts[index].id,
                }).then(json => {
                    console.log(json);
                    this.statusDel = true;
                    this.desserts.splice(index, 1);
                });
            }
        },

        close() {
            this.dialog = false
            //setTimeout(() => {
                this.editedItem = Object.assign({}, this.defaultItem)
                this.editedIndex = -1
                this.resetValidation()
            //}, 300)
        },

        save() {
            this.validate()
            if(this.valid && this.statusSend) {
                this.statusSend = false;
                this.$post(this.domain, {
                    rout: 'gnktAuditApiAll',
                    routAuditApi: 'setTheme',
                    theme: this.editedItem.theme,
                    intPass: this.editedItem.intPass,
                    dateMeas: this.editedItem.dateMeas,
                    measures: this.editedItem.measures,
                    ID: this.editedItem.id,
                    userID: this.$store.state.auth.info.id,
                    allUsers: this.allUsers,
                }).then(json => {
                    if(!json.error) {
                        this.close();
                        this.desserts = json;
                    }
                    this.statusSend = true;
                });
            }
        },
        validate () {
            this.$refs.form.validate()
        },
        reset () {
            this.$refs.form.reset()
        },
        resetValidation () {
            this.$refs.form.resetValidation()
        },
        changeList(item, page){
            let obj = {
                comp: 'auditConstructor',
                page: page,
                themeID: item.id,
                allUsers: ''+this.allUsers,
                themesList: this.desserts,
            };
            this.$emit('change-list',obj);
        },
        valueSwitch(){
            if(this.editedItem.measures==0){
                this.editedItem.dateMeas = '';
            }
        },
        showAll(){
            this.allUsers = !this.allUsers;

            //window.location.hash = window.location.hash.split('?')[0]+"?page="+this.dataObject.page+"&allUsers="+this.allUsers;

            //let get = window.location.hash.split('?')[1];
            //get = this.getToObject(get);
            //get.allUsers = this.allUsers;
            //window.location.hash = window.location.hash.split('?')[0] + this.objectToGet(get);

            this.initialize();
        },
        // objectToGet(obj){
        //     let str = "";
        //     for(let i in obj){
        //         str +=(str)?'&':'?';
        //         str +=i+'='+obj[i];
        //     }
        //     return str;
        // },
        // getToObject(get){
        //     let arr = get.split('&');
        //     let obj = {};
        //     for(let key in arr){
        //         let elem = arr[key].split('=');
        //         obj[elem[0]] = decodeURIComponent(elem[1]);
        //     }
        //     return obj;
        // }
    },
    data() {
        return {
            //domain: 'http://78.24.41.47:8083/ajax/gnktApi',
            statusDel: true,
            statusSend: true,
            search: '',
            head: [
                {text: 'ID',  value: 'id'},
                {text: 'Тема', value: 'theme'},
                {text: 'Автор', value: 'fio', disable: true},
                {text: 'Интервал прохождения (дни)', value: 'intPass'},
                {text: 'Мероприятия', value: 'measures'},
                {text: 'Срок мероприятий (дни)', value: 'dateMeas'},
                {text: 'Действия', value: 'actions', sortable: false, align:'right'},
            ],
            dialog: false,
            desserts: [],
            editedIndex: -1,
            editedItem: {
                theme: '',
                intPass: '',
                dateMeas: '',
                measures: 0,
                id: '',
            },
            defaultItem: {
                theme: '',
                intPass: '',
                dateMeas: '',
                measures: 0,
                id: '',
            },
            valid: false,
            nameRules: [
                v => !!v || 'Обязательное поле',
            ],
            showPreloader:false,
            measures:['Нет','Да'],
            allUsers: false,
            mysearch: false,
        };
    },
    computed: {
        formTitle() {
            return this.editedIndex === -1 ? 'Добавить тему' : 'Редактировать тему'
        },
        headers(){
            if(this.allUsers){
                this.head.find(item=>{if(item.value=='fio'){item.disable=false}});
            }else{
                this.head.find(item=>{if(item.value=='fio'){item.disable=true}});
            }
            return this.head.filter(item=>!item.disable);
        },
        access(){
            return this.$store.state.auth.info.access;
        },
    },

    watch: {
        dialog(val) {
            val || this.close()
        },
        access(val){
            if(val<2 && this.allUsers){
                this.showAll();
            }
        },
    },

    created() {
        this.allUsers = this.dataObject.allUsers;
        this.initialize();
    },
    template:/*html*/`        
<div style="position:relative;" class="d-flex flex-column">
    <div v-show="showPreloader" style="position: absolute;
   left: 0;
   right: 0;
   top:0;
   bottom: 0;
   background: rgba(255,255,255,0.7);
   display: flex;
   justify-content: center;
   align-items: center;
   z-index: 1;">
        <v-progress-circular
                indeterminate
                color="primary"
                size="50"
                width="4"
        ></v-progress-circular>
    </div>
    
        <v-toolbar flat class="flex-grow-0">
            <v-text-field
                    v-model="search"
                    __append-icon="mdi-magnify"
                    label="Поиск"
                    single-line
                    hide-details
                    outlined
                    dense
                    __clearable
                    style="width: 100%;"
                    v-if="mysearch"
            >
                <template v-slot:append>
                    <v-btn icon small style="margin-top: -2px;" @click="mysearch=false;search='';" color="primary"><v-icon>mdi-close</v-icon></v-btn>
                </template>
            </v-text-field>
            <template v-else>
            <v-toolbar-title>Темы</v-toolbar-title>

            <v-spacer></v-spacer>
                
            <v-btn v-if="!mysearch" icon small color="primary" class="ml-4" @click="mysearch=!mysearch">
                <v-icon>mdi-magnify</v-icon>
            </v-btn>
            <template v-if="$store.state.auth.info.access>=2">
                <v-btn class="ml-4" small @click="showAll" color="primary"
                       :loading="showPreloader" :disabled="showPreloader" icon>
                    <v-icon>
                        <template v-if="allUsers">mdi-account</template>
                        <template v-else>mdi-account-group</template>
                    </v-icon>
                </v-btn>
            </template>

            <v-dialog v-model="dialog" max-width="500px">
                <template v-slot:activator="{ on }">
                    <v-btn class="ml-4" small color="primary" v-on="on" icon><v-icon>mdi-plus-thick</v-icon></v-btn>
                </template>
                <v-card>
                    <v-card-title>
                        <span class="headline">{{ formTitle }}</span>
                    </v-card-title>

                    <v-card-text style="padding-bottom:0;">
                        <v-container style="padding:0;">
                            <v-form
                                    ref="form"
                                    v-model="valid"
                            >
                                <v-row>
                                    <v-col cols="12">
                                        <v-text-field :rules="nameRules" v-model="editedItem.theme" label="Тема"
                                                      required></v-text-field>
                                    </v-col>
                                    <v-col cols="12">
                                        <v-text-field type="number" min="1" v-model="editedItem.intPass"
                                                      label="Интервал прохождения (дни)"></v-text-field>
                                    </v-col>
                                    <v-col cols="12" sm="6">
                                        <v-switch v-model="editedItem.measures" @change="valueSwitch" true-value="1"
                                                  false-value="0" label="Мероприятия"></v-switch>
                                    </v-col>
                                    <v-col cols="12" sm="6">
                                        <v-text-field :disabled="editedItem.measures==0" type="number" min="1"
                                                      v-model="editedItem.dateMeas"
                                                      label="Срок устранения (дни)"></v-text-field>
                                    </v-col>
                                </v-row>
                            </v-form>
                        </v-container>
                    </v-card-text>

                    <v-card-actions>
                        <v-spacer></v-spacer>
                        <v-btn color="blue darken-1" text @click="close">Отмена</v-btn>
                        <v-btn color="blue darken-1" text @click="save" :disabled="!statusSend">
                            Сохранить
                            <v-progress-circular
                                    indeterminate
                                    color="default"
                                    size="15"
                                    width="2"
                                    style="margin-left:5px;"
                                    v-show="!statusSend"
                            ></v-progress-circular>
                        </v-btn>
                    </v-card-actions>
                </v-card>
            </v-dialog>
                <div/>
            </template>
        </v-toolbar>
        <v-divider/>
  <v-data-table
    :headers="headers"
    :items="desserts"
    :search="search"
    sort-by="id"
    sort-desc
    style="flex-grow:1;height: 0;overflow: auto;"
  >
      
    <template v-slot:item.measures="{ item }">
      {{ measures[item.measures] }}
    </template>
    <template v-slot:item.actions="{ item }">
    <div style="text-align: right;white-space: nowrap;">
      
          <v-menu bottom left>
            <template v-slot:activator="{ on, attrs }">
              <v-btn
                x-small
                icon
                v-bind="attrs"
                v-on="on"
              >
                <v-icon>mdi-dots-vertical</v-icon>
              </v-btn>
            </template>

            <v-list dense>
              <v-list-item @click="editItem(item)">
                  <v-list-item-title class="d-flex align-center"><v-icon class="mr-4">mdi-pencil</v-icon>Редактировать</v-list-item-title>
              </v-list-item>
              <v-list-item @click="changeList(item,'selectList')">
                  <v-list-item-title class="d-flex align-center"><v-icon class="mr-4">mdi-form-select</v-icon>Выпадающий список</v-list-item-title>
              </v-list-item>
              <v-list-item @click="changeList(item,'questionsList')">
                <v-list-item-title class="d-flex align-center"><v-icon class="mr-4">mdi-help-box</v-icon>Вопросы</v-list-item-title>
              </v-list-item>
              <v-list-item @click="changeList(item,'phoneList')">
                <v-list-item-title class="d-flex align-center"><v-icon class="mr-4">mdi-cellphone-android</v-icon>Телефоны</v-list-item-title>
              </v-list-item>
              <v-list-item @click="changeList(item,'userList')">
                <v-list-item-title class="d-flex align-center"><v-icon class="mr-4">mdi-account</v-icon>Пользователи</v-list-item-title>
              </v-list-item>
              <v-list-item @click="deleteItem(item)">
                <v-list-item-title class="d-flex align-center"><v-icon class="mr-4">mdi-delete</v-icon>Удалить</v-list-item-title>
              </v-list-item>
            </v-list>
          </v-menu>
      </div>
    </template>
    <template v-slot:no-data>
      Нет данных
    </template>
  </v-data-table>
  </div>
`
}