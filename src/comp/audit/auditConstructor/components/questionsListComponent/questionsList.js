export const questionsList = {
    name:'questionsList',
    props: {
        dataObject: Object,
        domain: String,
        idProject: Number,
    },
    methods: {
        initialize(start=false) {
            this.showPreloader = true;
            this.$post(this.domain, {
                rout: 'gnktAuditApiAll',
                routAuditApi: 'getQuestions',
                themeID: this.idProjectT,
            }).then(json => {
                this.desserts = json;
            }).catch(() => {
                console.error("Ошибка ответа сервера...");
            }).finally(() => {
                this.showPreloader = false;
            });
            // if (!start) {
            //     let get = window.location.hash.split('?')[1];
            //     get = this.getToObject(get);
            //     get.themeID = this.idProjectT;
            //     window.location.hash = window.location.hash.split('?')[0] + this.objectToGet(get);
            // }
        },
        // objectToGet(obj){
        //     let str = "";
        //     for(let i in obj){
        //         str +=(str)?'&':'?';
        //         str +=i+'='+obj[i];
        //     }
        //     return str;
        // },
        // getToObject(get){
        //     let arr = get.split('&');
        //     let obj = {};
        //     for(let key in arr){
        //         let elem = arr[key].split('=');
        //         obj[elem[0]] = decodeURIComponent(elem[1]);
        //     }
        //     return obj;
        // },

        editItem(item) {
            this.editedIndex = this.desserts.indexOf(item)
            this.editedItem = Object.assign({}, item)
            this.dialog = true
        },

        deleteItem(item) {
            if(confirm('Вы уверены что хотите удалить вопрос?')) {
                const index = this.desserts.indexOf(item)
                this.statusDel = false;
                this.$post(this.domain, {
                    rout: 'gnktAuditApiAll',
                    routAuditApi: 'deleteQuestion',
                    ID: this.desserts[index].id,
                }).then(json => {
                    console.log(json);
                    this.statusDel = true;
                    this.desserts.splice(index, 1);
                });
            }
        },

        close() {
            this.dialog = false
            //setTimeout(() => {
                this.editedItem = Object.assign({}, this.defaultItem)
                this.editedIndex = -1
                this.resetValidation()
            //}, 300)
        },

        save() {
            this.validate()
            if(this.valid && this.statusSend) {
                this.statusSend = false;
                this.$post(this.domain, {
                    rout: 'gnktAuditApiAll',
                    routAuditApi: 'setQuestion',
                    themeID: this.idProjectT,
                    question: this.editedItem.question,
                    description: this.editedItem.description,
                    answerDefault: (this.editedItem.answerDefault) ? this.editedItem.answerDefault : null,
                    photoReq:this.editedItem.photoReq,
                    commentReq:this.editedItem.commentReq,
                    ID: this.editedItem.id,
                }).then(json => {
                    if(!json.error) {
                        this.close();
                        this.desserts = json;
                    }
                    this.statusSend = true;
                });
            }
        },
        validate () {
            this.$refs.form.validate()
        },
        reset () {
            this.$refs.form.reset()
        },
        resetValidation () {
            this.$refs.form.resetValidation()
        },
        getAnswer(answer){
            for(let key in this.answers){
                if(this.answers[key].value==answer) {
                    return this.answers[key].text;
                }
            }
        },
        themeList(){
            let obj = {
                comp: 'auditConstructor',
                page: 'themesList',
                allUsers: ''+this.dataObject.allUsers,
            };
            this.$emit('change-list',obj);
        },
        loadThemes(){
            let arr = [];
            for (let i in this.dataObject.themesList) {
                arr.push({value: parseInt(this.dataObject.themesList[i].id), text: this.dataObject.themesList[i].theme});
            }
            this.allProjects = arr;
            this.showSelectProject = true;
        },
    },
    data() {
        return {
            //domain: 'http://78.24.41.47:8083/ajax/gnktApi',
            statusDel: true,
            statusSend: true,
            search: '',
            headers: [
                {text: 'ID',  value: 'id'},
                {text: 'Система / Компонент', value: 'question'},
                {text: 'Руководство для проверок', value: 'description'},
                {text: 'Ответ по умолчанию', value: 'answerDefault'},
                {text: 'Обязательный комментарий', value: 'commentReq'},
                {text: 'Обязательное фото', value: 'photoReq'},
                {text: 'Действия', value: 'actions', sortable: false, align:'right'},
            ],
            dialog: false,
            desserts: [],
            editedIndex: -1,
            editedItem: {
                question: '',
                description: '',
                answerDefault: '',
                photoReq: 0,
                commentReq: 0,
                id: '',
            },
            defaultItem: {
                question: '',
                description: '',
                answerDefault: '',
                photoReq: 0,
                commentReq: 0,
                id: '',
            },
            answers: [
                {value: '0',  text: 'Не выбран'},
                {value: '1',  text: 'Да'},
                {value: '2',  text: 'Нет'},
            ],
            valid: false,
            nameRules: [
                v => !!v || 'Обязательное поле',
            ],
            allProjects: [],
            idProjectT: false,
            showPreloader:false,
            preloadSelect:false,
            showSelectProject:false,
            boolean:['Нет','Да'],
            mysearch:false,
            showSelect:false,
        };
    },
    computed: {
        formTitle() {
            return this.editedIndex === -1 ? 'Добавить вопрос' : 'Редактировать вопрос'
        },
    },

    watch: {
        dialog(val) {
            val || this.close()
        },
    },

    created() {
        this.idProjectT = parseInt(this.dataObject.themeID);
        this.initialize(true);
        if(this.dataObject.themesList) {
            this.loadThemes();
        }else{
            this.preloadSelect = true;
            this.$post(this.domain, {
                rout: 'gnktAuditApiAll',
                routAuditApi: 'getThemes',
                userID: this.$store.state.auth.info.id,
                allUsers: this.dataObject.allUsers,
                idProject: this.idProject,
            }).then(json => {
                this.dataObject.themesList = json;
                this.loadThemes();
            }).catch(() => {
                console.error("Ошибка ответа сервера...");
            }).finally(() => {
                this.preloadSelect = false;
            });
        }
    },
    template:/*html*/`

<div style="position:relative;" class="d-flex flex-column">
    <div v-show="showPreloader" style="position: absolute;
   left: 0;
   right: 0;
   top:0;
   bottom: 0;
   background: rgba(255,255,255,0.7);
   display: flex;
   justify-content: center;
   align-items: center;
   z-index: 1;">
        <v-progress-circular
                indeterminate
                color="primary"
                size="50"
                width="4"
        ></v-progress-circular>
    </div>
    <v-dialog v-model="dialog" max-width="500px">
        <v-card>
            <v-card-title>
                <span class="headline">{{ formTitle }}</span>
            </v-card-title>

            <v-card-text style="padding-bottom:0;">
                <v-container style="padding:0;">
                    <v-form
                            ref="form"
                            v-model="valid"
                    >
                        <v-row>
                            <v-col cols="12">
                                <v-text-field :rules="nameRules" v-model="editedItem.question" label="Система / Компонент" required></v-text-field>
                            </v-col>
                            <v-col cols="12">
                                <v-textarea
                                        v-model="editedItem.description"
                                        label="Руководство для проверок"
                                        filled
                                ></v-textarea>
                            </v-col>
                            <v-col cols="12">
                                <v-select
                                        v-model="editedItem.answerDefault"
                                        :items="answers"
                                        dense
                                        label="Ответ по умолчанию"
                                        required
                                        clearable
                                ></v-select>
                            </v-col>

                            <v-col cols="12" sm="6">
                                <v-switch v-model="editedItem.commentReq" true-value="1" false-value="0" label="Обязательный комментарий"></v-switch>
                            </v-col>
                            <v-col cols="12" sm="6">
                                <v-switch v-model="editedItem.photoReq" true-value="1" false-value="0" label="Обязательное фото"></v-switch>
                            </v-col>
                        </v-row>


                    </v-form>
                </v-container>
            </v-card-text>

            <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="blue darken-1" text @click="close">Отмена</v-btn>
                <v-btn color="blue darken-1" text @click="save" :disabled="!statusSend">
                    Сохранить
                    <v-progress-circular
                            indeterminate
                            color="default"
                            size="15"
                            width="2"
                            style="margin-left:5px;"
                            v-show="!statusSend"
                    ></v-progress-circular>
                </v-btn>
            </v-card-actions>
        </v-card>
    </v-dialog>
    <v-toolbar flat color="white" class="flex-grow-0">
        <v-text-field
                v-model="search"
                __append-icon="mdi-magnify"
                label="Поиск"
                single-line
                hide-details
                outlined
                dense
                __clearable
                style="width: 100%;"
                v-if="mysearch"
        >
            <template v-slot:append>
                <v-btn icon small style="margin-top: -2px;" @click="mysearch=false;search='';" color="primary"><v-icon>mdi-close</v-icon></v-btn>
            </template>
        </v-text-field>
        <template v-else-if="showSelect">
            <v-select
                    v-if="showSelectProject"
                    :items="allProjects"
                    label="Тема"
                    v-model="idProjectT"
                    @change="initialize()"
                    hide-details
                    dense
                    outlined
            >
                <template v-slot:append>
                    <v-btn icon small style="margin-top: -2px;" @click="showSelect=false;" color="primary"><v-icon>mdi-close</v-icon></v-btn>
                </template>
            </v-select>
            <v-progress-circular
                    class="ml-4"
                    v-if="preloadSelect"
                    indeterminate
                    color="green"
                    size="25"
                    width="2"
            ></v-progress-circular>
        </template>
        <template v-else>
            <div/>
            <v-btn
                    small
                    color="primary"
                    @click="themeList()"
                    icon
                    class="mr-4"
            >
                <v-icon>mdi-arrow-left</v-icon>
            </v-btn>
            <v-toolbar-title>Вопросы</v-toolbar-title>
            <v-spacer></v-spacer>

            <!--            <v-text-field-->
            <!--                    v-model="search"-->
            <!--                    append-icon="mdi-magnify"-->
            <!--                    label="Поиск"-->
            <!--                    single-line-->
            <!--                    hide-details-->
            <!--            ></v-text-field>-->
            <v-btn icon small color="primary" class="ml-4" @click="showSelect=true">
                <v-icon>mdi-form-select</v-icon>
            </v-btn>
            <v-btn icon small color="primary" class="ml-4" @click="mysearch=true">
                <v-icon>mdi-magnify</v-icon>
            </v-btn>
            <v-btn class="ml-4" small color="primary" @click="dialog=true" icon>
                <v-icon>mdi-plus-thick</v-icon>
            </v-btn>
            <div/>
        </template>
    </v-toolbar>
    <v-divider/>
  <v-data-table
    :headers="headers"
    :items="desserts"
    :search="search"
    sort-by="id"
    sort-desc
    style="flex-grow:1;height: 0;overflow: auto;"
  >
    <template v-slot:item.actions="{ item }">
        <div style="text-align: right;">
      <v-icon
        small
        class="mr-2"
        @click="editItem(item)"
        title="Редактировать"
      >
        mdi-pencil
      </v-icon>
      <v-icon
        small
        @click="deleteItem(item)"
        :disabled="!statusDel"
        title="Удалить"
      >
        mdi-delete
      </v-icon>
        </div>
    </template>
    <template v-slot:item.answerDefault="{ item }">
      {{ getAnswer(item.answerDefault) }}
    </template>
    <template v-slot:item.commentReq="{ item }">
      {{ boolean[item.commentReq] }}
    </template>
    <template v-slot:item.photoReq="{ item }">
      {{ boolean[item.photoReq] }}
    </template>
    <template v-slot:no-data>
      Нет данных
    </template>
  </v-data-table>
  </div>
`
}