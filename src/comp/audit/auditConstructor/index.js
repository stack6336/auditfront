import {themesList} from "./components/themesListComponent/themesList.js";
import {questionsList} from "./components/questionsListComponent/questionsList.js";
import {phoneList} from "./components/phoneListComponent/phoneList.js";
import {userList} from "./components/userListComponent/userList.js";
import {selectList} from "./components/selectListComponent/selectList.js";

export const auditConstructor = {
    components: {
        themesList,
        questionsList,
        phoneList,
        userList,
        selectList,
    },
    props:{
        domain:String,
        idProject: Number,
    },
    methods: {
        loadPage(obj){
            this.switchComponent = ('page' in obj)?obj.page:'themesList';
            if(this.$store.state.auth.info.access>=2)obj.allUsers = (obj.allUsers === "true") ? true : false;
            else delete obj.allUsers;
            this.dataObject = obj;
            // let getObj = JSON.parse(JSON.stringify(obj));
            // delete getObj.themesList;
            //window.location.hash = window.location.hash.split('?')[0] + this.objectToGet(getObj);
        },
        // objectToGet(obj){
        //     let str = "";
        //     for(let i in obj){
        //         str +=(str)?'&':'?';
        //         str +=i+'='+obj[i];
        //     }
        //     return str;
        // },
        // getToObject(get){
        //     let arr = get.split('&');
        //     let obj = {};
        //     for(let key in arr){
        //         let elem = arr[key].split('=');
        //         obj[elem[0]] = decodeURIComponent(elem[1]);
        //     }
        //     return obj;
        // }
    },
    data() {
        return {
            switchComponent: '',
            dataObject: {},
        };
    },
    mounted() {
        // let get = window.location.hash.split('?')[1];
        // if(get){
        //     let getObj = this.getToObject(get);
            let getObj = {};
            this.loadPage(getObj,true);
        //}
        // this.$root.addCustomMenu([
        //     {
        //         title: "Конструктор аудита",
        //         action:()=>{
        //             this.$emit("viewPage","auditConstructor");
        //         },
        //     },
        //     {
        //         title: "Результаты аудита",
        //         action:()=>{
        //             this.$emit("viewPage","auditResult");
        //         },
        //     },
        //     {
        //         title: "Чат",
        //         action:()=>{
        //             this.$emit("viewPage","auditChat");
        //         },
        //     },
        // ]);
    },
    beforeDestroy() {
        //this.$root.removeCustomMenu();
    },
    template:/*html*/`        
<component :is="switchComponent" :domain="domain" :dataObject="dataObject" @change-list="loadPage" :idProject="idProject"></component>
`
}