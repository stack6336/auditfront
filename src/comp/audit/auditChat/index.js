export const auditChat = {
    mounted() {
        this.$root.addCustomMenu([
            {
                title: "Конструктор аудита",
                action:()=>{
                    this.$emit("viewPage","auditConstructor");
                },
            },
            {
                title: "Результаты аудита",
                action:()=>{
                    this.$emit("viewPage","auditResult");
                },
            },
            {
                title: "Чат",
                action:()=>{
                    this.$emit("viewPage","auditChat");
                },
            },
        ]);
    },
    beforeDestroy(){
        this.$root.removeCustomMenu();
    },
    template:/*html*/`
<div style="height: 100%;">
<hc-chat tile flat color-header="primary" dark></hc-chat>
</div>
`
}