export const startMenu = {
    methods: {},
    data() {
        return {
            listMenu:[
                {
                    title: "Конструктор аудита",
                    action:()=>{
                        this.$emit("viewPage","auditConstructor");
                    },
                },
                {
                    title: "Результаты аудита",
                    action:()=>{
                        this.$emit("viewPage","auditResult");
                    },
                },
                // {
                //     title: "Чат",
                //     action:()=>{
                //         this.$emit("viewPage","auditChat");
                //     },
                // },
            ],
        };
    },
    mounted(){
        //this.$root.addCustomMenu(this.listMenu);
    },
    beforeDestroy() {
        //this.$root.removeCustomMenu();
    },
    template:/*html*/`
<div class="d-flex justify-center align-center" style="height: 100%;">
    <div class="d-inline-flex flex-wrap justify-center align-start mx-auto st-tile-menu-index">
        <v-card 
            v-for="(item, index) of listMenu"
            :key="index"
            @click="item.action"
            class="ma-2 d-flex justify-center align-center st-tile-menu-index-item flex-column"
            width="300px" 
            height="100px"
            color="#252D41"
            dark
            ripple
            hover  
        >
            <v-card-title><v-icon left v-if="item.icon" color="#FFF">{{item.icon}}</v-icon><span>{{item.title}}</span></v-card-title>
        </v-card>
    </div>
</div>
`
}