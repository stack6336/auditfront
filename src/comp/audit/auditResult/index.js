import {resultList} from "./components/resultListComponent/resultList.js";
import {answerList} from "./components/answerListComponent/answerList.js";

export const auditResult = {
    components: {
        resultList,
        answerList,
    },
    props:{
        domain:String,
        idProject: Number,
    },
    methods: {
        loadPage(obj,start=false){
            this.switchComponent = ('page' in obj)?obj.page:'resultList';
            this.dataObject = obj;
            //if(!start) window.location.hash = window.location.hash.split('?')[0] + this.objectToGet(obj);
        },
        objectToGet(obj){
            let str = "";
            for(let i in obj){
                str +=(str)?'&':'?';
                str +=i+'='+obj[i];
            }
            return str;
        },
        getToObject(get){
            let arr = get.split('&');
            let obj = {};
            for(let key in arr){
                let elem = arr[key].split('=');
                obj[elem[0]] = decodeURIComponent(elem[1]);
            }
            return obj;
        }
    },
    data() {
        return {
            switchComponent: '',
            dataObject: {},
        };
    },
    mounted() {
        // let get = window.location.hash.split('?')[1];
        // if(get){
        //     let getObj = this.getToObject(get);
        let getObj = {};
            this.loadPage(getObj,true);
        //}
        // this.$root.addCustomMenu([
        //     {
        //         title: "Конструктор аудита",
        //         action:()=>{
        //             this.$emit("viewPage","auditConstructor");
        //         },
        //     },
        //     {
        //         title: "Результаты аудита",
        //         action:()=>{
        //             this.$emit("viewPage","auditResult");
        //         },
        //     },
        //     {
        //         title: "Чат",
        //         action:()=>{
        //             this.$emit("viewPage","auditChat");
        //         },
        //     },
        // ]);
    },
    beforeDestroy() {
        // this.$root.removeCustomMenu();
    },
    template:/*html*/`

<component :is="switchComponent" :domain="domain" :dataObject="dataObject" @change-list="loadPage" :idProject="idProject"></component>

`
}