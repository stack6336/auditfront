import {mapOS} from "./mapOS.js";
export const resultList = {
    components:{
        mapOS,
    },
    props:{
        domain:String,
        idProject: Number,
    },
    computed:{
        filter(){
            return (this.year || this.theme>0)?true:false;
        },
        years(){
            let arr = [];
            let nowYear = new Date().getFullYear();
            let minYear = nowYear;
            for(let i in this.desserts){
                let thisYear = new Date(this.desserts[i].dateTime).getFullYear();
                if(thisYear<minYear) minYear=thisYear;
            }
            for(let i = nowYear;i>=minYear;i--){
                arr.push({text:i,value:i});
            }
            return arr;
        },
        months(){
            let arr = [];
            let month = (new Date().getFullYear()==this.year) ? new Date().getMonth()+1 : 12;
            for(let i = 1;i<=month;i++){
                arr.push({text:this.monthsList[i-1],value:i});
            }
            return arr;
        },
        days(){
            let colDays = (new Date().getMonth()+1==this.month) ? new Date().getDate() : 32 - new Date(this.year, this.month-1, 32).getDate();
            let arr = [];
            for(let i = 1;i<=colDays;i++){
                arr.push({text:i,value:i});
            }
            return arr;
        },
        themes(){
            let arr = [];
            for(let i in this.desserts){
                arr.push({text: this.desserts[i].theme, value: this.desserts[i].themeID});
            }
            return arr;
        },
        resList(){
            return this.desserts.filter(item=>
                (!this.theme || item.themeID==this.theme)
                && (!this.year || new Date(item.dateTime).getFullYear()==this.year)
                && (!this.month || new Date(item.dateTime).getMonth()+1==this.month)
                && (!this.day || new Date(item.dateTime).getDate()==this.day)
            );
        },
    },
    watch:{
        year(val){
            if(!val)this.month=false;
        },
        month(val){
            if(!val)this.day=false;
        },
        mobile(val){
            if(val){
                this.left = 60;
                this.right = 40;
            }else{
                this.left = 40;
                this.right = 60;
            }
        },
    },
    methods: {
        resizeMap(){
            window.dispatchEvent(new Event('resize'));
        },
        getDevice(){
            this.mobile = window.innerWidth<=1199;
        },
        initialize(start=false) {
            this.showPreloader = true;
            this.$post(this.domain, {
                rout: 'gnktAuditApiAll',
                routAuditApi: 'getAllResult',
                userID: this.$store.state.auth.info.id,
            }).then(json => {
                if(this.destroy) return false;
                //console.log(json);
                this.desserts = this.getStatusRepair(json);
                if(start===true){
                    this.$nextTick(()=>{
                        for(let i in this.$refs.table.openCache){
                            this.$refs.table.openCache[i] = false;
                        }
                    });
                }
            }).catch(() => {
                console.error("Ошибка ответа сервера...");
            }).finally(() => {
                this.showPreloader = false;
            });
        },
        answerList(item){
            let obj = {
                comp: 'auditResult',
                page: 'answerList',
                resultID: item.id,
            };
            this.$emit('change-list',obj);
        },
        deleteItem(item) {
            if(confirm('Вы уверены что хотите удалить результат?')) {
                const index = this.desserts.indexOf(item)
                this.statusDel = false;
                this.$post(this.domain, {
                    rout: 'gnktAuditApiAll',
                    routAuditApi: 'hiddenResult',
                    ID: item.id,
                }).then(json => {
                    console.log(json);
                    this.desserts.splice(index, 1);
                    //this.setDateFilter();
                }).catch(() => {
                    console.error("Ошибка ответа сервера...");
                }).finally(() => {
                    this.statusDel = true;
                });
            }
        },
        editItem(item) {
            this.editedIndex = this.desserts.indexOf(item);
            this.editedItem = Object.assign({}, item);
            this.dialog = true;
        },
        save() {
            this.validate()
            if(this.valid && this.statusSend) {
                this.statusSend = false;
                this.$post(this.domain, {
                    rout: 'gnktAuditApiAll',
                    routAuditApi: 'updResult',
                    commentTheme: this.editedItem.commentTheme,
                    ID: this.editedItem.id,
                }).then(json => {
                    if(json.set) {
                        this.desserts[this.editedIndex].commentTheme = this.editedItem.commentTheme;
                        this.close();
                    }
                    this.statusSend = true;
                });
            }
        },
        validate () {
            this.$refs.form.validate()
        },
        reset () {
            this.$refs.form.reset()
        },
        resetValidation () {
            this.$refs.form.resetValidation()
        },
        close() {
            this.dialog = false
            //setTimeout(() => {
            this.editedItem = Object.assign({}, this.defaultItem)
            this.editedIndex = -1
            this.resetValidation()
            //}, 300)
        },
        getMap(item){
            let marker = [item.lat.replace(/,/, '.'), item.long.replace(/,/, '.')];
            this.centerMap = marker;
            this.markers = [{
                id: item.id,
                coords: marker,
                visible: true
            }];
        },
        getStatusRepair(json){
            for(let i in json) {
                if(!json[i].enterprise)json[i].enterprise='Без подразделения';
                let obj = false;
                if (json[i].dateTimeRepair) {
                    obj = {text: 'Исправлено', color: 'green'};
                } else {
                    if (json[i].measures==1 && json[i].repair == 0) {
                        let actual = true;
                        if (json[i].dateMeas) {
                            let days = (new Date() - new Date(json[i].dateTime + ' GMT+4')) / 86400000;
                            if (days > json[i].dateMeas) {
                                actual = false;
                            }
                        }
                        if (actual) {
                            obj = {text: 'Ожидание', color: 'blue'};
                        } else {
                            obj = {text: 'Просрочено', color: 'red'};
                        }
                    }
                }
                json[i].statusRepair = obj;
                //Устанавливаем флаг для раскрывающихся панелей
                let item = this.desserts.filter(item=>item.id==json[i].id);
                if(item.length && item[0].showDet){
                    json[i].showDet = true;
                }else{
                    json[i].showDet = false;
                }
            }
            return json;
        },
    },
    data() {
        return {
            //domain: 'http://78.24.41.47:8083/ajax/gnktApi',
            search: '',
            headers: [
                {text: 'Название аудита', value: 'commentTheme'},
                {text: 'Тема аудита', value: 'theme'},
                {text: 'Объект', value: 'selected'},
                {text: 'Организация', value: 'enterprise', filterable: true,},
                {text: 'ФИО', value: 'fio'},
                {text: 'Дата', value: 'dateTime'},
                {text: 'Исправность', value: 'percent'},
                {text: 'Исправность после мероприятий', value: 'percentRepair'},
                {text: 'Дата мероприятия', value: 'dateTimeRepair'},
                {text: 'Статус мероприятия', value: 'statusRepair'},
            ],
            desserts: [],
            showPreloader: false,
            statusDel: true,
            lat: '',
            long: '',
            centerMap: [55.03, 82.92],
            markers: [],

            editedIndex: -1,
            editedItem: {
                commentTheme: '',
                id: '',
            },
            defaultItem: {
                commentTheme: '',
                id: '',
            },
            statusSend: true,
            dialog: false,
            valid: false,
            nameRules: [
                v => !!v || 'Обязательное поле',
            ],
            heightTable: '50px',
            year: false,
            month: false,
            day: false,
            monthsList: [
                'Январь',
                'Февраль',
                'Март',
                'Апрель',
                'Май',
                'Июнь',
                'Июль',
                'Август',
                'Сентябрь',
                'Октябрь',
                'Ноябрь',
                'Декабрь',
            ],
            //resList: [],
            theme: false,
            showFilter: false,
            destroy:false,
            mysearch: false,
            mobile: false,
            left: 40,
            right: 60,
        };
    },
    created() {
        this.initialize(true);
    },
    beforeUpdate(){},
    mounted() {
        window.addEventListener('resize', this.getDevice);
        this.resizeMap();
    },
    beforeDestroy() {
        window.removeEventListener('resize', this.getDevice);
        this.destroy = true;
    },
    template:/*html*/`
        <div>
<splitpanes @resize="resizeMap" :class="{'hc_split_mobile':mobile}" :horizontal="mobile">
    <pane :size="left" min-size="30" max-size="70">
        <div class="hc_col_box d-flex flex-column">

            <div v-show="showPreloader" style="position: absolute;
   left: 0;
   right: 0;
   top:0;
   bottom: 0;
   background: rgba(255,255,255,0.7);
   display: flex;
   justify-content: center;
   align-items: center;
   z-index: 1;">
                <v-progress-circular
                        indeterminate
                        color="primary"
                        size="50"
                        width="4"
                ></v-progress-circular>
            </div>


            <div>
                <v-toolbar flat>
                    <template v-if="!mysearch">
                        <v-toolbar-title>Результаты</v-toolbar-title>
                        <v-spacer></v-spacer>
                    </template>
                    <!--                      <v-text-field-->
                    <!--                              v-model="search"-->
                    <!--                              append-icon="mdi-magnify"-->
                    <!--                              label="Поиск"-->
                    <!--                              single-line-->
                    <!--                              hide-details-->
                    <!--                              class="ml-2"-->
                    <!--                      ></v-text-field>-->


                    <v-text-field
                            v-model="search"
                            __append-icon="mdi-magnify"
                            label="Поиск"
                            single-line
                            hide-details
                            outlined
                            dense
                            __clearable
                            v-show="mysearch"
                    >
                        <template v-slot:append>
                            <v-btn icon small style="margin-top: -2px;" @click="mysearch=false;search='';"
                                   color="primary">
                                <v-icon>mdi-close</v-icon>
                            </v-btn>
                        </template>
                    </v-text-field>
                    <v-btn v-if="!mysearch" icon small color="primary" class="ml-4" @click="mysearch=!mysearch">
                        <v-icon>mdi-magnify</v-icon>
                    </v-btn>


                    <v-btn icon small @click="showFilter=!showFilter;" color="primary" class="ml-2">
                        <v-badge color="success" dot :value="filter">
                            <v-icon v-if="showFilter">mdi-filter-remove-outline</v-icon>
                            <v-icon v-else>mdi-filter-outline</v-icon>
                        </v-badge>
                    </v-btn>


                    <v-btn icon small @click="initialize" color="primary" class="ml-2">
                        <v-icon :class="{'anim_rot': showPreloader}">mdi-sync</v-icon>
                    </v-btn>

                    <v-dialog v-model="dialog" max-width="500px">
                        <v-card>
                            <v-card-title>
                                <span class="headline">Редактировать результат</span>
                            </v-card-title>

                            <v-card-text style="padding-bottom:0;">
                                <v-container style="padding:0;">
                                    <v-form
                                            ref="form"
                                            v-model="valid"
                                    >
                                        <v-row>
                                            <v-col cols="12">
                                                <v-text-field :rules="nameRules" v-model="editedItem.commentTheme"
                                                              label="Название аудита" required></v-text-field>
                                            </v-col>
                                        </v-row>
                                    </v-form>
                                </v-container>
                            </v-card-text>

                            <v-card-actions>
                                <v-spacer></v-spacer>
                                <v-btn color="blue darken-1" text @click="close">Отмена</v-btn>
                                <v-btn color="blue darken-1" text @click="save" :disabled="!statusSend">
                                    Сохранить
                                    <v-progress-circular
                                            indeterminate
                                            color="default"
                                            size="15"
                                            width="2"
                                            style="margin-left:5px;"
                                            v-show="!statusSend"
                                    ></v-progress-circular>
                                </v-btn>
                            </v-card-actions>
                        </v-card>
                    </v-dialog>


                </v-toolbar>
                <div v-if="showFilter" fluid class="pa-4">
                    <v-row>
                        <v-col cols="12" class="pt-0">
                            <v-select
                                    label="Тема"
                                    hide-details
                                    outlined
                                    dense
                                    :items="themes"
                                    v-model="theme"
                                    clearable
                            ></v-select>
                        </v-col>
                        <v-col cols="12" sm="4" class="pt-0">
                            <v-select
                                    label="Год"
                                    hide-details
                                    outlined
                                    dense
                                    :items="years"
                                    v-model="year"
                                    clearable
                            ></v-select>
                        </v-col>
                        <v-col cols="12" sm="4" class="pt-0">
                            <v-select
                                    label="Месяц"
                                    hide-details
                                    outlined
                                    dense
                                    :items="months"
                                    v-model="month"
                                    :disabled="!year"
                                    clearable
                            ></v-select>
                        </v-col>
                        <v-col cols="12" sm="4" class="pt-0">
                            <v-select
                                    label="День"
                                    hide-details
                                    outlined
                                    dense
                                    :items="days"
                                    v-model="day"
                                    :disabled="!month"
                                    clearable
                            ></v-select>
                        </v-col>
                    </v-row>
                </div>
                <v-divider></v-divider>
            </div>


            <div style="flex-grow: 1;height:0;overflow: auto;">
                <v-data-table
                        :headers="headers"
                        :items="resList"
                        :search="search"
                        sort-by="id"
                        sort-desc
                        hide-default-header
                        hide-default-footer
                        fixed-header
                        disable-pagination
                        ref="table"
                        __group-by="enterprise"
                >
                    <!--                                    <template v-slot:group.header="{group,headers,isOpen,toggle}">-->
                    <!--                                        <td>-->
                    <!--                                            <div class="d-flex align-center">-->
                    <!--                                            <span style="font-size: 16px;">{{group}}</span>-->
                    <!--                                            <v-spacer></v-spacer>-->
                    <!--                                            <v-btn icon @click="toggle">-->
                    <!--                                                <v-icon v-if="isOpen">mdi-minus</v-icon>-->
                    <!--                                                <v-icon v-else>mdi-plus</v-icon>-->
                    <!--                                            </v-btn>-->
                    <!--                                            </div>-->
                    <!--                                        </td>-->
                    <!--                                    </template>-->
                    <template v-slot:item="{ item }">

                        <div
                                class="px-4 py-3 d-flex hc_my_tr align-center"
                                :class="{ 'grey lighten-3': item.showDet }"
                        >
                            <div class="mr-4">
                                <div style="font-size: 14px;">{{item.theme}}</div>
                                <div v-if="item.commentTheme || item.selected" class="caption mb-1 text--secondary">
                                    <template v-if="item.commentTheme">{{item.commentTheme}}</template>
                                    <template v-if="item.commentTheme && item.selected"> |</template>
                                    <template v-if="item.selected">{{item.selected}}</template>
                                </div>
                                <div class="text--disabled caption">{{item.dateTime.slice(0, -4)}}</div>
                            </div>
                            <v-spacer></v-spacer>

                            <v-icon
                                    v-if="item.lat && item.long"
                                    small
                                    class="mr-2"
                                    @click="getMap(item)"
                                    title="Местоположение"
                            >
                                mdi-map-marker
                            </v-icon>
                            <v-icon
                                    small
                                    class="mr-2"
                                    @click="answerList(item)"
                                    title="Список вопросов"
                            >
                                mdi-help-box
                            </v-icon>
                            <v-icon
                                    v-if="item.author==$store.state.auth.info.id"
                                    small
                                    class="mr-2"
                                    @click="editItem(item)"
                                    title="Редактировать"
                            >
                                mdi-pencil
                            </v-icon>
                            <v-icon
                                    v-if="item.author==$store.state.auth.info.id"
                                    small
                                    class="mr-2"
                                    @click="deleteItem(item)"
                                    :disabled="!statusDel"
                                    title="Удалить"
                            >
                                mdi-delete
                            </v-icon>
                            <v-icon
                                    small
                                    @click="item.showDet = !item.showDet"
                                    title="Детали"
                            >
                                <template v-if="!item.showDet">mdi-plus-thick</template>
                                <template v-else>mdi-minus</template>
                            </v-icon>

                        </div>
                        <v-divider/>

                        <div v-show="item.showDet">
                            <div class="py-2 hc_open_exp">
                                <div class="d-flex py-2 px-4 hc_line_result" v-for="head in headers">
                                    <div class="mr-4 subtitle-2">{{head.text}}</div>
                                    <v-spacer></v-spacer>
                                    <div class="text--secondary text-right body-2">
                                        <template v-if="head.value == 'percent'">
                                            <template v-if="item.percent">{{ item.percent }}%</template>
                                        </template>
                                        <template v-else-if="head.value == 'percentRepair'">
                                            <template v-if="item.percentRepair">{{ item.percentRepair }}%</template>
                                        </template>
                                        <template v-else-if="head.value == 'statusRepair'">
                                            <v-chip v-if="item.statusRepair" small dark
                                                    :color="item.statusRepair.color">{{ item.statusRepair.text }}</v-chip>
                                        </template>
                                        <template v-else>{{item[head.value]}}</template>
                                    </div>
                                </div>
                            </div>
                            <v-divider/>
                        </div>


                    </template>


                    <!--    <template v-slot:item.percent="{ item }">-->
                    <!--      <template v-if="item.percent">{{ item.percent }}%</template>-->
                    <!--    </template>-->
                    <!--    <template v-slot:item.percentRepair="{ item }">-->
                    <!--      <template v-if="item.percentRepair">{{ item.percentRepair }}%</template>-->
                    <!--    </template>-->
                    <!--      <template v-slot:item.statusRepair="{ item }">-->
                    <!--          <v-chip v-if="item.statusRepair" small dark :color="item.statusRepair.color">{{ item.statusRepair.text }}</v-chip>-->
                    <!--      </template>-->
                    <!--    <template v-slot:item.actions="{ item }">-->
                    <!--    <div class="d-flex justify-end flex-nowrap" style="">-->
                    <!--      <v-icon-->
                    <!--        v-if="item.lat && item.long"-->
                    <!--        small-->
                    <!--        class="mr-2"-->
                    <!--        @click="getMap(item)"-->
                    <!--        title="Местоположение"-->
                    <!--      >-->
                    <!--        mdi-map-marker-->
                    <!--      </v-icon>-->
                    <!--      <v-icon-->
                    <!--        small-->
                    <!--        class="mr-2"-->
                    <!--        @click="answerList(item)"-->
                    <!--        title="Список вопросов"-->
                    <!--      >-->
                    <!--        mdi-help-box-->
                    <!--      </v-icon>-->
                    <!--      <v-icon-->
                    <!--        small-->
                    <!--        @click="deleteItem(item)"-->
                    <!--        :disabled="!statusDel"-->
                    <!--        title="Удалить"-->
                    <!--      >-->
                    <!--        mdi-delete-->
                    <!--      </v-icon>-->
                    <!--    </div>-->
                    <!--    </template>-->


                    <template v-slot:no-data>
                        Нет данных
                    </template>
                </v-data-table>
            </div>

        </div>
    </pane>
    <pane :size="right" min-size="30" max-size="70">
        <div class="hc_col_box">
            <mapOS
                    :center="centerMap"
                    :zoom="12"
                    :markers="markers"
            >
            </mapOS>
        </div>
    </pane>
</splitpanes>
        </div>
`
}