import L from 'leaflet';
import * as Vue2Leaflet from 'vue2-leaflet';
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
    iconUrl: require("leaflet/dist/images/marker-icon.png"),
    shadowUrl: require("leaflet/dist/images/marker-shadow.png")
});
export const mapOS = {
    name: 'mapOS',
    props: {
        markers: Array,
        circles: Array,
        newCircles: Array,
        nearCircles: Array,
        center: Array,
        zoom: Number,
        lines: Array,
        urlTrack: String,
        markPopup: String,
        param: String
    },
    watch: {},
    components: {
        'v-map': Vue2Leaflet.LMap,
        'v-tilelayer': Vue2Leaflet.LTileLayer,
        'v-tracklayer': Vue2Leaflet.LTileLayer,
        'v-marker': Vue2Leaflet.LMarker,
        'v-circle': Vue2Leaflet.LCircle,
        'v-nearcircle': Vue2Leaflet.LCircle,
        'v-newcircle': Vue2Leaflet.LCircle,
        'v-line': Vue2Leaflet.LPolyline,
        'v-control': Vue2Leaflet.LControl,
        'v-popup': Vue2Leaflet.LPopup,
    },
    mounted() {
        this.sm = this.param;
        this.$emit('map', this.$refs.map)
        this.$refs.map.$on('zoomstart', e => {
        });
    },
    methods: {
        onScroll(e) {
            this.options = {};
        },
        parseV(val) {
            return parseInt(val);
        },
        keypress(e) {
            if (e.originalEvent.key === 'Control') {
            }
        },
        markerClick(value) {

            this.$emit('markerClick', value.item)
        },
        circleClick(value) {
            this.$emit('circleClick', value)
        },
        circleNearClick(value) {
            this.$emit('circleNearClick', value)
        },
        circleNewClick(value) {
            this.$emit('circleNewClick', value)
        },
        mapClick(value) {
            this.$emit('mapClick', value)
        },
        getCenter(value) {
            this.$emit('getCenter', value)
        },
        getZoom(value) {
            this.$emit('getZoom', value)
        },
    },
    data() {
        return {
            options: {scrollWheelZoom: true},
            trFile: "{z}/{x}/{y}.png",
            color: 'green',
            sid: null,
            rad: 20,
            sm: "",
            trackUrl: '',
            url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
        };
    },
// :options="{scrollWheelZoom:false}"
    template:/*html*/`
        <v-map @keydown="keypress" :options="options" ref="map" @update:center="getCenter" @update:zoom="getZoom" @click ="mapClick" style="z-index: 0;" class="map" id='mapOnline' :zoom="zoom" :center="center">
                <v-tilelayer :url="url" :attribution="attribution"></v-tilelayer>
                <v-tracklayer v-if="urlTrack" :url="urlTrack+trFile"></v-tracklayer>
                <v-circle  v-for="circle in circles"
                            :key="circle.idGZ"
                            :lat-lng="[circle.gzLat, circle.gzLong]"
                            :radius="parseV(circle.rad)"
                            :color="circle.color"
                            @click ="circleClick(circle)"
                            >
                </v-circle>
                <v-control position="bottomright">
                             <slot name="cntr-br"></slot>
                </v-control>
                <v-control position="bottomleft">
                             <slot name="cntr-bl"></slot>
                </v-control>
                <v-control position="topright">
                        <slot name="cntr-tr"></slot>
                </v-control>
                <v-control position="topleft">
                             <slot name="cntr-tl"></slot>
                </v-control>
                <v-nearcircle  v-for="circle in nearCircles"
                            :key="circle.idGZ"
                            :lat-lng="[circle.lat, circle.long]"
                            :radius="parseV(circle.rad)"
                            :color="'blue'"
                             @click ="circleNearClick(circle)"
                            >
                </v-nearcircle>
                <v-newcircle  v-for="circle in newCircles"
                            :key="circle.grID+'_1'"
                            :lat-lng="[circle.gzLat, circle.gzLong]"
                            :radius="parseV(circle.rad)"
                            :color="circle.color"
                             @click ="circleNewClick(circle)"
                            >
                </v-newcircle>
                <v-marker v-for="marker in markers" 
                           :visible="marker.visible"
                           :key="marker.id"
                           :lat-lng="marker.coords"
                            @click = "markerClick(marker)"
                            >
                  <v-popup v-if="false" open="false" >
                          <slot name="markrPopup" ></slot>
                 </v-popup>
                </v-marker>
                <v-line v-for="line in lines" 
                           :visible="line.visible"
                           :key="line.id"
                           :color="line.color"
                           :fillColor="line.fillColor"
                           :weight="line.weight"
                           :fillOpacity="line.fillOpacity"
                           :latLngs="line.latLngs"
                            @click = "markerClick(marker)"
                            >
                </v-line>

        </v-map> 
`

}