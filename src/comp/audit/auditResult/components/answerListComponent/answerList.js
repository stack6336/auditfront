export const answerList = {
    props: {
        dataObject: Object,
        domain: String,
    },
    methods: {
        initialize() {
            this.showPreloader = true;
            this.$post(this.domain, {
                rout: 'gnktAuditApiAll',
                routAuditApi: 'getResult',
                resultID: this.dataObject.resultID,
            }).then(json => {
                //console.log(json);
                this.desserts = json.quest;
                this.curRes = this.getStatusRepair(json.info);
            }).catch(() => {
                console.error("Ошибка ответа сервера...");
            }).finally(() => {
                this.showPreloader = false;
            });
        },
        getStatusRepair(json){
                if(!json.enterprise)json.enterprise='Без подразделения';
                let obj = false;
                if (json.dateTimeRepair) {
                    obj = {text: 'Исправлено', color: 'green'};
                } else {
                    if (json.measures==1 && json.repair == 0) {
                        let actual = true;
                        if (json.dateMeas) {
                            let days = (new Date() - new Date(json.dateTime + ' GMT+4')) / 86400000;
                            if (days > json.dateMeas) {
                                actual = false;
                            }
                        }
                        if (actual) {
                            obj = {text: 'Ожидание', color: 'blue'};
                        } else {
                            obj = {text: 'Просрочено', color: 'red'};
                        }
                    }
                }
                json.statusRepair = obj;
            return json;
        },
        message(text,color='error'){
            this.textNotice = text;
            this.colorNotice = color;
            this.notice = true;
        },
        assignAnswers(){
            if(confirm('Отправить выделенные вопросы на "Повторный аудит"?')){
                if(!this.showPreloader) {
                    this.showPreloader = true;
                    this.$post(this.domain, {
                        rout: 'gnktAuditApiAll',
                        routAuditApi: 'assignRepeatAudit',
                        results: JSON.stringify(this.selected),
                    }).then(json => {
                        console.log(json);
                        if(json.set){
                            this.selected = [];
                            this.message('Вопросы успешно назначены!','success');
                        }else{
                            this.message('Ошибка на сервере!','error');
                        }
                    }).catch(() => {
                        this.message('Ошибка ответа сервера...','error');
                    }).finally(() => {
                        this.showPreloader = false;
                    });
                }
            }
        },
        getAnswer(answer){
            for(let key in this.answers){
                if(this.answers[key].value==answer) {
                    return this.answers[key].text;
                }
            }
        },
        resultList(){
            let obj = {
                comp: 'auditResult',
                page: 'resultList',
            };
            this.$emit('change-list',obj);
        },
        showPhoto(path,photos){
            if(photos){
                this.loadPath = true;
                this.$post(this.domain, {
                    rout: 'gnktAuditApiAll',
                    routAuditApi: 'getPhotoPath',
                    photos: photos,
                }).then(json => {
                    let arr = [];
                    for(let i in json){
                        arr.push({src: this.server + json[i].path});
                    }
                    this.images = arr;
                    this.visible = true;
                }).catch(() => {
                    console.error("Ошибка ответа сервера...");
                }).finally(() => {
                    this.loadPath = false;
                });
            }else{
                this.images = [{src: this.server + path}];
                this.visible = true;
            }
        },
        handleHide() {
            this.visible = false;
        },
    },
    data() {
        return {
            server: 'http://194.87.99.148:8389',
            search: '',
            headers: [
                {text: 'ID',  value: 'id'},
                {text: 'Вопрос', value: 'question'},
                {text: 'Ответ', value: 'answer'},
                {text: 'Комментарий', value: 'comment'},
                {text: 'Фото', value: 'photoPath', sortable: false},
                {text: 'Ответ мероприятия', value: 'answerRepair'},
                {text: 'Комментарий мероприятия', value: 'commentRepair'},
                {text: 'Фото мероприятия', value: 'photoPathRepair', sortable: false},

                {text: 'Повторный ответ', value: 'answerRepeat'},
                {text: 'Повторный комментарий', value: 'commentRepeat'},
                {text: 'Повторное фото', value: 'photoPathRepeat', sortable: false},
            ],
            desserts: [],
            answers: [
                {value: '0',  text: 'Не выбран'},
                {value: '1',  text: 'Да'},
                {value: '2',  text: 'Нет'},
            ],
            showPreloader:false,
            loadPath:false,
            images:[],
            visible: false,
            index:0,
            selected:[],
            colorNotice: 'error',
            textNotice: '',
            notice: false,
            curRes:[],
            dialogInfo:false,
            headersInfo: [
                {text: 'Название аудита', value: 'commentTheme'},
                {text: 'Тема аудита', value: 'theme'},
                {text: 'Объект', value: 'selected'},
                {text: 'Организация', value: 'enterprise', filterable: true,},
                {text: 'ФИО', value: 'fio'},
                {text: 'Дата', value: 'dateTime'},
                {text: 'Исправность', value: 'percent'},
                {text: 'Исправность после мероприятий', value: 'percentRepair'},
                {text: 'Дата мероприятия', value: 'dateTimeRepair'},
                {text: 'Статус мероприятия', value: 'statusRepair'},
            ],
            mysearch:false,
        };
    },
    created() {
        this.initialize();
    },
    template:/*html*/`<div>
        <v-snackbar class="my_snackbar" :color="colorNotice" right top v-model="notice">
            {{ textNotice }}
            <template v-slot:action="{ attrs }">
                <v-btn dark icon @click="notice = false"><v-icon small>mdi-close</v-icon></v-btn>
            </template>
        </v-snackbar>
<template>
<div style="position:relative;height: 100%;" class="d-flex flex-column">
    <div v-show="showPreloader" style="position: absolute;
   left: 0;
   right: 0;
   top:0;
   bottom: 0;
   background: rgba(255,255,255,0.7);
   display: flex;
   justify-content: center;
   align-items: center;
   z-index: 1;">
        <v-progress-circular
                indeterminate
                color="primary"
                size="50"
                width="4"
        ></v-progress-circular>
    </div>
        <v-toolbar flat color="white" class="flex-grow-0">
            <v-text-field
                    v-model="search"
                    __append-icon="mdi-magnify"
                    label="Поиск"
                    single-line
                    hide-details
                    outlined
                    dense
                    __clearable
                    v-if="mysearch"
            >
                <template v-slot:append>
                    <v-btn icon small style="margin-top: -2px;" @click="mysearch=false;search='';" color="primary"><v-icon>mdi-close</v-icon></v-btn>
                </template>
            </v-text-field>
            <template v-else>
                <div/>
                <v-btn
                        small
                        color="primary"
                        @click="resultList()"
                        icon
                        class="mr-4"
                >
                    <v-icon>mdi-arrow-left</v-icon>
                </v-btn>
                <v-toolbar-title class="pl-0">{{curRes.theme}}</v-toolbar-title>
                <v-spacer></v-spacer>
                <v-btn
                        small
                        class="ml-4"
                        color="primary"
                        @click="assignAnswers"
                        :disabled="!selected.length"
                        v-if="selected.length"
                        icon
                >
                    <v-icon>mdi-repeat</v-icon>
                </v-btn>
                <v-btn v-if="!mysearch" icon small color="primary" class="ml-4" @click="mysearch=!mysearch">
                    <v-icon>mdi-magnify</v-icon>
                </v-btn>
                <v-dialog
                        v-model="dialogInfo"
                        max-width="600"
                >
                    <template v-slot:activator="{ on, attrs }">
                        <v-btn class="ml-4" icon small v-bind="attrs" v-on="on" color="primary"><v-icon>mdi-information-outline</v-icon></v-btn>
                    </template>
                    <v-card>
                        <v-card-title>Информация о результате</v-card-title>
                        <v-card-text>
                            <div class="d-flex mt-4 hc_line_result" v-for="head in headersInfo">
                                <div class="mr-4 font-weight-bold">{{head.text}}</div>
                                <v-spacer></v-spacer>
                                <div class="text-right">
                                    <template v-if="head.value == 'percent'">
                                        <template v-if="curRes.percent">{{ curRes.percent }}%</template>
                                    </template>
                                    <template v-else-if="head.value == 'percentRepair'">
                                        <template v-if="curRes.percentRepair">{{ curRes.percentRepair }}%</template>
                                    </template>
                                    <template v-else-if="head.value == 'statusRepair'">
                                        <v-chip v-if="curRes.statusRepair" small dark :color="curRes.statusRepair.color">{{ curRes.statusRepair.text }}</v-chip>
                                    </template>
                                    <template v-else>{{curRes[head.value]}}</template>
                                </div>
                            </div>
                        </v-card-text>
                        <v-card-actions>
                            <v-spacer></v-spacer>
                            <v-btn text color="primary" @click="dialogInfo=false">Закрыть</v-btn>
                        </v-card-actions>
                    </v-card>
                </v-dialog>
                <div/>

                <!--          <v-btn-->
                <!--                  small-->
                <!--                  color="primary"-->
                <!--                  @click="resultList()"-->
                <!--          >-->
                <!--              <v-icon class="mr-2" small>mdi-arrow-left-bold</v-icon>-->
                <!--              Назад-->
                <!--          </v-btn>-->
            </template>
        </v-toolbar>
        <v-divider/>
  <v-data-table
    :headers="headers"
    :items="desserts"
    :search="search"
    show-select
    v-model="selected"
    style="flex-grow:1;height: 0;overflow: auto;"
  >
    <template v-slot:item.photoPath="{ item }">
        <div v-if="item.photoPath" class="d-flex justify-end py-2">
            <div style="width:70px;position:relative;">
                <div v-show="loadPath" class="hc_disabled_skin"></div>
                <v-img
                        aspect-ratio="1.4"
                        @click="showPhoto(item.photoPath,item.photos)"
                        :src="server+item.photoPath"
                        style="cursor:zoom-in;"
                ></v-img>
            </div>
        </div>
    </template>
      <template v-slot:item.photoPathRepair="{ item }">
          <div v-if="item.photoPathRepair" class="d-flex justify-end py-2">
              <div style="width:70px;position:relative;">
                  <div v-show="loadPath" class="hc_disabled_skin"></div>
                  <v-img
                          aspect-ratio="1.4"
                          @click="showPhoto(item.photoPathRepair,item.photosRepair)"
                          :src="server+item.photoPathRepair"
                          style="cursor:zoom-in;"
                  ></v-img>
              </div>
          </div>
      </template>
      <template v-slot:item.photoPathRepeat="{ item }">
          <div v-if="item.photoPathRepeat" class="d-flex justify-end py-2">
              <div style="width:70px;position:relative;">
                  <div v-show="loadPath" class="hc_disabled_skin"></div>
                  <v-img
                          aspect-ratio="1.4"
                          @click="showPhoto(item.photoPathRepeat,item.photosRepeat)"
                          :src="server+item.photoPathRepeat"
                          style="cursor:zoom-in;"
                  ></v-img>
              </div>
          </div>
      </template>
    <template v-slot:item.answer="{ item }">
      {{ getAnswer(item.answer) }}
    </template>
      <template v-slot:item.answerRepair="{ item }">
          {{ getAnswer(item.answerRepair) }}
      </template>
      <template v-slot:item.answerRepeat="{ item }">
          {{ getAnswer(item.answerRepeat) }}
      </template>
    <template v-slot:no-data>
      Нет данных
    </template>
  </v-data-table>
  </div>
</template>
    <template>
        <vue-easy-lightbox
                :visible="visible"
                :imgs="images"
                :index="index"
                @hide="handleHide"
        ></vue-easy-lightbox>
    </template>

</div>
`
}