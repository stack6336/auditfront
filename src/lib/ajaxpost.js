let lastTimeResetApi = Date.now();
function toGET(url, data, token) {
    let link = url.split("#");
    link = link[0].split("?");
    let param = link.slice(1);
    link = link[0];
    param = param.map(i => i.split("&").map(i => i.split("=")))[0] || [];
    if (token) {
        data.hash = token;
    }
    for (let i in data) {
        if (Array.isArray(data[i])) {
            for (let j in data[i]) {
                param.push([i + "[]", data[i][j]].map(encodeURI));
            }
        } else {
            param.push([i, data[i]].map(encodeURI))
        }
    }
    param = param.map(i => i.join("=")).join("&");

    return [link, param].filter(i => i).join("?");
}

function toPOST(url, data, token) {
    let diff = (Date.now()-lastTimeResetApi)/3600000;
    if(diff>2){
        localStorage.startApi = JSON.stringify(Object.values(window.myvue.$store.state.api));
        lastTimeResetApi = Date.now();
    }
    let form = new FormData();
    if(localStorage.startApi && !!JSON.parse(localStorage.startApi).find(i=>i==url)) data.auth = localStorage.auth;
    if(token) data.hash = token;
    //if(url == 'http://194.87.99.148:8080' && sessionStorage.HCSSID)data.HCSSID = sessionStorage.HCSSID;
    for (let i in data) {
        if (Array.isArray(data[i])) {
            for (let j in data[i]) {
                form.append(i + "[]", data[i][j]);
            }
        } else {
            form.append(i, data[i]);
        }
    }
    return form;
}

function ajax(url, options) {
    return new Promise((resolve, reject) => {
        let token = (localStorage.auth) ? JSON.parse(localStorage.auth).hash : "";
        fetch(toGET(url, options.get || {}, (options.method == "POST" || options.post) ? undefined : token), {
            credentials: "include",
            // headers: {
            //     accept: "application/json"
            // },
            body: options.post ? toPOST(url, options.post, token) : null,
            method: (options.method == "POST" || options.post) ? "POST" : "GET",
            mode: "cors"
        }).then(res => res.text()).then(text => {
            try {
                let json = JSON.parse(text.trim());
                if (json && json.user && json.user.token) {
                    /*localStorage.token = */
                    token = json.user.token;
                }
                if (localStorage.startApi) {
                    let arr = JSON.parse(localStorage.startApi);
                    let indexURL = arr.findIndex(i => i == url);
                    if (indexURL !== -1) {
                        arr.splice(indexURL, 1);
                        if (arr.length) localStorage.startApi = JSON.stringify(arr);
                        else localStorage.removeItem('startApi');
                    }
                }
                resolve(json);
            } catch (e) {
                reject(e);
            }
        }).catch(reject);
    });
}
let importedJS = [];
function importJS(src){
    if(importedJS.indexOf(src) == 0){
        return Promise.resolve();
    }else {
        return new Promise((resolve, reject) => {
            importedJS.push(src);
            let s = document.createElement("SCRIPT");
            s.onload = resolve;
            s.onerror = reject;
            s.src = src;
            document.head.appendChild(s);
        });
    }
}

export default {
    install(Vue) {

        /**
         * POST запрос
         *
         * @param url - адрес запроса
         * @param postParam - параметры запроса
         * @return Promise
         */
        Vue.prototype.$post = (url, postParam) => ajax(url, {method: "POST", post: postParam});

        /**
         * GET запрос
         *
         * @param url - адрес запроса
         * @param getParam - параметры запроса
         * @return Promise
         */
        Vue.prototype.$get = (url, getParam) => ajax(url, {method: "GET", get: getParam});

        /**
         * AJAX запрос
         * @param url - адрес запроса
         * @param options - параметры запроса
         * @return Promise
         */
        Vue.prototype.$ajax = ajax;
        Vue.prototype.$importJS = importJS;

        Vue.prototype.$message = (text,color='error')=>{
            window.myvue.$store.dispatch('set', {
                prop: 'message',
                value: {
                    text: text,
                    color: color
                }
            });
        };
    }
};
