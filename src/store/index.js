import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        auth: {},
        api: {
            urlAuth: 'http://194.87.99.148:8080',
            urlApi: 'http://194.87.99.148:8389',
        },
    },
    getters:{
        get: state => state
    },
    mutations: {
        set:(state,payload)=>state[payload.prop] = payload.value
    },
    actions: {
        set:({commit}, payload)=>commit("set", payload)
    },
});