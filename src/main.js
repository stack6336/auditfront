import Vue from 'vue'
import App from './App.vue'
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/dist/vuetify.min.css'
import Vuetify from "vuetify";
import ajaxpost from "./lib/ajaxpost.js";
import ru from 'vuetify/es5/locale/ru';
import hcAudit from './comp/audit';
import './css/audit.css';
import Lightbox from 'vue-easy-lightbox';
import 'leaflet/dist/leaflet.css';
import store from './store';
import hcFullscreen from "./comp/hcFullscreen";
import { Splitpanes, Pane } from 'splitpanes';
import 'splitpanes/dist/splitpanes.css';

Vue.config.productionTip = false;

Vue.use(Vuetify);
Vue.use(ajaxpost);
Vue.use(Lightbox);

Vue.component('hcAudit', hcAudit);
Vue.component('hcFullscreen', hcFullscreen);
Vue.component('splitpanes', Splitpanes);
Vue.component('pane', Pane);

const vuetify = new Vuetify({
    lang: {
        locales: { ru },
        current: 'ru',
    },
    icons: {
        iconfont: "mdi"
    },
});

window.myvue = new Vue({
    vuetify,
    store,
    render: h => h(App)
}).$mount('#app');


